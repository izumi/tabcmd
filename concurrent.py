#!/usr/bin/python
# coding=UTF-8

from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Pango

from command import Command
from procbar import CommandView
from monitor import CommandExecView
from interact import CommandExecTerminal


class PaneTab(Gtk.Box):
    CLOSE_TAB_CSS_STRING = """
        .button {
            padding: 0;
            /* background: rgb(250.0, 0.0, 0.0); */
            -GtkWidget-focus-line-width: 0px;
            -GtkWidget-focus-padding: 0px;
            -GtkButton-default-border: 0px;
            -GtkButton-default-outside-border: 0px;
            -GtkButton-inner-border: 0px; /* Before Gtk+ 3.4 */
        }
    """
    CSSPROV = Gtk.CssProvider()
    CSSPROV.load_from_data(CLOSE_TAB_CSS_STRING)

    def __init__(self, caption):
        super(PaneTab, self).__init__(orientation=Gtk.Orientation.HORIZONTAL)
        #self.pack_start(icon, False, True, 0)
        tl = Gtk.Label(caption)
        tl.set_single_line_mode(True);
        tl.set_alignment(0.0, 0.5);
        tl.set_ellipsize(Pango.EllipsizeMode.END)
        tl.set_width_chars(6)
        self.pack_start(tl, True, True, 0)
        btn = Gtk.Button()
        # gtk.Image objects cannot be shared
        btn.add(PaneTab._create_close_image())
        btn.set_relief(Gtk.ReliefStyle.NONE)
        btn.set_focus_on_click(False)
        btn.get_style_context().add_provider(PaneTab.CSSPROV,
                        Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        self.pack_end(btn, False, False, 0)
        self.show_all()

        self._close_button = btn

    @staticmethod
    def _create_close_image():
        img = Gtk.Image()
        img.set_from_stock(Gtk.STOCK_CLOSE, Gtk.IconSize.MENU)
        img.show()
        return img

    def child_started(self, widget, pid):
        self._close_button.set_sensitive(False)
        self._close_button.set_tooltip_text("Process is alive")

    def child_exited(self, widget, pid, stat):
        self._close_button.set_sensitive(True)
        self._close_button.set_tooltip_text("Close tab")


class ConcurrentCommands(Gtk.Notebook):
    def __init__(self):
        super(ConcurrentCommands, self).__init__()
        self._accel_grp = self.__accel_group()

    def __accel_group(self):
        grp = Gtk.AccelGroup()
        for i in "123456789":
            grp.connect(ord(i), Gdk.ModifierType.MOD1_MASK,
                            Gtk.AccelFlags.VISIBLE,
                            lambda ag, ac, k, mod: self._tab_selected(ac, k))
        return grp

    def _tab_selected(self, widget, key):
        idx = self._nth_visible_page(int(chr(key)) - 1)
        if 0 <= idx:
            self.set_current_page(idx)

    def _nth_visible_page(self, idx):
        c = 0
        for i in xrange(self.get_n_pages()):
            if self.get_nth_page(i).get_visible():
                if c == idx:
                    break
                c += 1
        else:
            i = -1
        return i

    def accel_group(self):
        return self._accel_grp

    def _add_pane(self, view, cmd):
        tab = PaneTab(cmd._name)
        view.set_status_change_callbacks(tab.child_started, tab.child_exited)
        tab.set_tooltip_text(" ".join(cmd._cmdline))
        idx = self.append_page(view, tab)
        view.show_all()
        if idx >= 0:
            self.set_tab_reorderable(view, True)
            self.set_tab_detachable(view, True)
            self.set_current_page(idx)
            tab._close_button.connect("clicked",
                    lambda button: self._delete_view(view))

    def _delete_view(self, view):
        view.stop_monitoring()
        idx = self.page_num(view)
        if idx >= 0:
            self.remove_page(idx)

    def apply_visibility(self, show, active):
        act = (lambda view, data: view.show_all()) if show else \
                    (lambda view, data: view.hide())
        if active is None:
            cond = lambda view, data: True
        elif active:
            cond = lambda view, data: view.is_running()
        else:
            cond = lambda view, data: not view.is_running()
        return self.apply_action(act, cond)

    def apply_action(self, action, cond=None, data=None):
        c = 0
        for i in xrange(self.get_n_pages()):
            view = self.get_nth_page(i)
            if cond is None or cond(view, data):
                action(view, data)
                c += 1
        return c

    def prune_pages(self):
        c = 0
        cond = lambda view, data: not view.is_running()
        # For stability save target pages first
        pages = list(self.select_pages(cond))
        for view in pages:
            self._delete_view(view)
            c += 1
        return c

    def select_pages(self, cond, data=None):
        for i in xrange(self.get_n_pages()):
            view = self.get_nth_page(i)
            if cond(view, data):
                yield view

    def show_processes(self, cmds, toplevel=None):
        for cmd in cmds:
            # Do not invoke recursively
            if isinstance(cmd, Command):
                self.show_process(cmd, toplevel)

    def show_process(self, cmd, toplevel=None):
        if cmd._interactive:
            self._exec_win = CommandExecTerminal()
        else:
            self._exec_win = CommandExecView()
        view = CommandView(cmd._cmdline[0], self._exec_win)
        if toplevel is not None:
            # Install specific keybindings if any
            accgrp = self._exec_win.key_bindings()
            if accgrp is not None:
                toplevel.add_accel_group(accgrp)

        self._add_pane(view, cmd)
        view.start_command(cmd)

    def num_running(self):
        """Number of currently running processes."""
        c = 0
        for i in xrange(self.get_n_pages()):
            view = self.get_nth_page(i)
            if view.is_running():
                c += 1
        return c

    @staticmethod
    def _delete_page(notes, widget):
        idx = notes.page_num(widget)
        if idx >= 0:
            notes.remove_page(idx)
