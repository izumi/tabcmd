#!/usr/bin/python
# coding=UTF-8

import sys
import os

#
# Importation of Gtk fails if some of commandline args or environments
# are wrong (Debian 7.x wheezy).
# E.g. $0 --display=:1 will raise RuntimeError if display :1 is not available.
# This error is RAISED DURING IMPORTATION.
# Therefore to appropriately handle this error the application needs to
# check sys.argv and environments before importing Gtk.
# This means that it is neccessary to check Gtk specific commandline args
# and envs without Gtk module.
#
from gi.repository import Gtk
from gi.repository import Gdk
#import cairo
#import pango


class StockGtkApp(object):
    MAX_DEBUG_LEVEL = 3
    TheApp = None

    def __init__(self, *args):
        super(StockGtkApp, self).__init__()
        self._debug_level = 0
        window = Gtk.Window(Gtk.WindowType.TOPLEVEL)
        window.set_default_size(600, 600)
#        self._bind_keys(window)

        # widget.sig_delete(userdata)
        window.connect("delete-event", self._delete)
        # Actual application termination
        window.connect("destroy", self.quit)

        self._setup(window, *args)

        window.show_all()
        assert StockGtkApp.TheApp is None
        # Store the first application
        StockGtkApp.TheApp = self

    def _bind_keys(self, window):
        accel_group = Gtk.AccelGroup()
        window.add_accel_group(accel_group)
        accel_group.connect(ord('W'), Gdk.ModifierType.CONTROL_MASK,
                Gtk.AccelFlags.VISIBLE, self._delete)
        accel_group.connect(ord('Q'), Gdk.ModifierType.CONTROL_MASK,
                Gtk.AccelFlags.VISIBLE, self._delete)
        accel_group.connect(ord('R'), Gdk.ModifierType.MOD1_MASK,
                Gtk.AccelFlags.VISIBLE,
                lambda ag, ac, k, mod: self.re_exec())
        accel_group.connect(ord('D'), Gdk.ModifierType.CONTROL_MASK,
                Gtk.AccelFlags.VISIBLE,
                lambda ag, ac, k, mod: self.incr_debug())

    def _setup(self, window):
        #raise NotImplementedError("_setup")
        window.set_default_size(400, 300)
        window.add(Gtk.Label("StockGtkApp._setup() method is not overridden.\n"
                        "Needs to get implemented"))

    def _delete(self, *args):
        # Return whether to continue or destroy.
        # Actual destroy will occur afterwards
        return not self._terminatable()

    def terminate(self):
        if self._terminatable():
            self.quit(None)
        return True # Still alive

    def _terminatable(self, replace=False):
        """Determine whether the application can terminate."""
        return True

    def quit(self, arg):
        """Exit the main loop."""
        Gtk.main_quit()

    def re_exec(self):
        # Now no arguments
        if self._terminatable(True):
            argv = sys.argv
            argv.insert(0, 'python')
            #print "re_exec: {0}".format(str(argv))
            os.execvp(argv[0], argv)

    def run(self):
        Gtk.main()

    def incr_debug(self):
        self._debug_level += 1
        self._debug_level %= self.MAX_DEBUG_LEVEL

    def debug_level(self):
        return self._debug_level


def app_debug_level():
    return 0 if StockGtkApp.TheApp is None else StockGtkApp.TheApp.debug_level()


if __name__ == '__main__':
    #import sys

    class MyApp(StockGtkApp):
        def _setup(self, window):
            window.set_default_size(400, 300)
            window.add(Gtk.Label("_setup"))

    MyApp().run()
