#!/usr/bin/python
# coding=UTF-8

from os import O_NONBLOCK
import fcntl

from gi.repository import Gtk

class GtkUtils(object):
    @staticmethod
    def combobox_prepend_unique(combobox, column, text):
        model = combobox.get_model()
        GtkUtils.remove_duplicated_row(model, column, text)
        model.prepend([text])

    @staticmethod
    def remove_duplicated_row(model, column, value):
        removed = False

        it = model.get_iter_first()
        while it is not None:
            if model.get_value(it, column) == value:
                break
            it = model.iter_next(it)

        if it is not None:
            model.remove(it)
            removed = True

        return removed

    @staticmethod
    def create_stock_menu_item(label, stockid):
        menuitem = Gtk.ImageMenuItem(stockid)
        menuitem.set_label(label)
        menuitem.set_always_show_image(True)
        return menuitem


class FileUtils(object):
    @staticmethod
    def set_blocking_mode(fd, mode):
        flags = fcntl.fcntl(fd, fcntl.F_GETFL)
        if mode:
            flags &= ~O_NONBLOCK
        else:
            flags |= O_NONBLOCK
        fcntl.fcntl(fd, fcntl.F_SETFL, flags)
