#!/usr/bin/python
# coding=UTF-8

from gi.repository import GLib
from gi.repository import Gtk
from gi.repository import Vte

from procbar import ExecViewBase


class CommandExecTerminal(ExecViewBase):
    CLEAR_SEQ = b"\x1b[H\x1b[2J"        # for xterm
    PTY_LOG_FLAG = (Vte.PtyFlags.NO_LASTLOG | Vte.PtyFlags.NO_UTMP
                        | Vte.PtyFlags.NO_WTMP)

    def __init__(self):
        super(CommandExecTerminal, self).__init__()
        terminal = Vte.Terminal()
        scrolledwindow = Gtk.ScrolledWindow(None, terminal.get_vadjustment())
        scrolledwindow.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.ALWAYS)
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)
        scrolledwindow.add(terminal)
        self._terminal = terminal
        terminal.set_scrollback_lines(1024)
        self._exec_win = scrolledwindow
#        self._exec_win = terminal

    def monitor_widget(self):
        return self._exec_win

    def start_command(self, cmd):
        self._cmd = cmd
        pid = None

        # Precise version number is unknown
        try:
            if (Vte.MAJOR_VERSION, Vte.MINOR_VERSION) >= (0, 38):
                pid = invoke_in_terminal(self._terminal, cmd)
            else:
                stat, pid = self._terminal.fork_command_full(CommandExecTerminal.PTY_LOG_FLAG,
                                    cmd._workdir,
                                    cmd._cmdline,
                                    [],
                                    GLib.SpawnFlags.DEFAULT
                                        | GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                                    None, None)
        except GLib.GError as ex: # or as GObject.GError
            print "Error: ", ex

        print "PID:", pid

        return pid

    def clear_output(self):
        self._terminal.feed(CommandExecTerminal.CLEAR_SEQ)


def invoke_in_terminal(term, cmd):
    # Recent VteTerminal (ver 0.48+) provides spawn_async() method
    # which seems to be equivalent to this function.
    pty = None
    try:
        pty = term.pty_new_sync(CommandExecTerminal.PTY_LOG_FLAG, None)
    except GLib.GError as ex:
        print "Pty creation error:", ex
        raise ex

    try:
        # The interface of GLib.spawn_async() is different from
        # original g_spawn_async().
        #     spawn_async(argv, envp=None, working_directory=None,
        #                flags=0, child_setup=None, user_data=None,
        #                standard_input=None, standard_output=None,
        #                standard_error=None) -> (pid, stdin, stdout, stderr)
        # Passing envp=None raises TypeError:
        #    gi._glib.spawn_async: second argument must be a sequence of strings
        # So it is necessary to use keyword args to pass None as a
        # value of envp.
        (pid, i, o, e) = GLib.spawn_async(cmd._cmdline,
                                        working_directory=cmd._workdir,
                                        flags=GLib.SpawnFlags.DO_NOT_REAP_CHILD,
                                        child_setup=Vte.Pty.child_setup,
                                        user_data=pty)
    except GLib.GError as ex: # or as GObject.GError
        print "Spawn error:", ex
        raise ex

    print "Spawned", pid
    term.set_pty(None)
    term.set_pty(pty)
    term.watch_child(pid)
    return pid
