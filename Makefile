
	 PROG = tabcmd

	 SRCS = appwin.py command.py cmdmenu.py \
		concurrent.py procbar.py info.py \
		interact.py monitor.py misc.py

db: tags cscope.out

check:
	pyflakes $(PROG) $(SRCS)

tags: $(PROG) $(SRCS)
	ctags $(PROG) $(SRCS)

cscope.out: $(PROG) $(SRCS)
	pycscope $(PROG) $(SRCS)

archive:
	git archive --prefix $(PROG)-0.1/ -o $(PROG)-0.1.tar.gz HEAD

