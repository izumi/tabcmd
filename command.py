#!/usr/bin/python
# coding=UTF-8

import os

class Command(object):
    BUF_LINE = 'L'
    BUF_INTACT = 'intact'

    WORK_DIR = os.path.expanduser("~")
    SHELL = os.environ.get("SHELL", "/bin/sh")

    def __init__(self, name, cmdline, **kwargs):
        super(Command, self).__init__()
        self._name = name
        self._cmdline = cmdline
        workdir = kwargs.get("workdir")
        self._workdir = Command.WORK_DIR if workdir is None \
                else os.path.expanduser(workdir)
        self._env = kwargs.get("environ")
        self._exec = kwargs.get("executable", cmdline[0])
        self._interactive = kwargs.get("interactive", False)
        self._stdin = kwargs.get("stdin")	# file handle
        if self._interactive:
            # _outbuf, _errbuf are not used
            self._outbuf = kwargs.get("outbuf", Command.BUF_INTACT)
            self._errbuf = kwargs.get("errbuf", Command.BUF_INTACT)
        else:
            self._outbuf = kwargs.get("outbuf", 0)
            self._errbuf = kwargs.get("errbuf", 0)

        self._close_on_success = kwargs.get("close_on_success", False)
        self._success_delay = kwargs.get("close_delay", 0)
        self._error_delay = kwargs.get("error_delay", None)

    def close_delay(self, code):
        return self._success_delay if code == 0 else self._error_delay

    def command_line(self, sep=None):
        return self._cmdline if sep is None else sep.join(self._cmdline)

    def adapted_command(self):
        """Modify the command line to control the output stream buffering."""
        # Interactive command do not require this.
        # To see the output as soon as the running command writes something,
        # it may be necessary to supress output buffering.
        # Prepend stdbuf command for this purpose.
        # Necessary workaround depends on the command.
        # Some commands require this, others do not.
        # Some cannot be affected by stdbuf.
        # Usually stdbuf with -oL and -eL options should be enough.
        c = 0
        prefix = ['stdbuf']
        for (opt, optval) in (('o', self._outbuf), ('e', self._errbuf)):
            if optval == Command.BUF_INTACT:
                continue
            else:
                prefix.append("-{0}{1:}".format(opt, optval))
                c += 1

        return prefix + self._cmdline if c > 0 else self._cmdline

    @staticmethod
    def shell_command():
        return Command(os.path.basename(Command.SHELL), [Command.SHELL],
                        workdir=".", interactive=True)


class CommandList(object):
    """Simply named sequence."""
    def __init__(self, name, commands):
        super(CommandList, self).__init__()
        self._name = name
        self._list = commands

    def name(self):
        return self._name

    def __iter__(self):
        return iter(self._list)
