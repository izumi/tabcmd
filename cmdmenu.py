#!/usr/bin/python
# coding=UTF-8

import sys
import os

from command import Command, CommandList

Slowcats = CommandList("Slowcats", (
            Command("slowcat", ["./slowcat"], workdir=".",
                    outbuf=Command.BUF_INTACT, errbuf=Command.BUF_INTACT),
            Command("slowcat", ["./slowcat"], workdir=".",
                    outbuf=Command.BUF_INTACT, errbuf=Command.BUF_INTACT),
        ))

SubList = CommandList("PyGtk", (
            Command("Twitter", ["./umgqineko"],
                        workdir="~/gadgetry/gui/umgqineko"),
            Command("Icons", ["/usr/bin/python", "tlbar-img.py"],
                    workdir="~/trial/pygobj-gallery"),
            Command("Tabcmd", sys.argv, workdir="."),)
        )

CommandMenu = CommandList("CommandMenu", (
    Command("db check", ["make", "db", "check"], workdir="."),
    Command("pstree", ["pstree", "-Ual"]),
    Command("FakeFar", ["./fakefar", "tun4"], workdir="~/gadgetry/fakefar"),
    # slowcat needs no buffering manipulation
    Command("slowcat", ["./slowcat"], workdir=".",
            outbuf=Command.BUF_INTACT, errbuf=Command.BUF_INTACT),
    Command("RSS", ["/usr/bin/make", "install"], workdir="~/trial/rdfconv"),
    # Since ls exits immediately, no buffering manipulation is necessary.
    Command("ls ~",  ["ls", "-lF"],
            outbuf=Command.BUF_INTACT, errbuf=Command.BUF_INTACT),
    Slowcats, SubList,
    ))

Shell = os.environ.get("SHELL", "/bin/zsh")

DirList = CommandList("Directories", (
            Command("umgqineko", [Shell],
                        workdir="~/gadgetry/gui/umgqineko", interactive=True),
            Command("tabcmd", [Shell],
                        workdir="~/gadgetry/gui/tabcmd", interactive=True),
            Command("cerezo", [Shell],
                        workdir="~/gadgetry/gui/cerezo", interactive=True),
            )
        )

TerminalCommandMenu = CommandList("TerminalCommandMenu", (
    Command("shell", [Shell],
            workdir=".", interactive=True),
    Command("ipython", ["/usr/bin/ipython"], workdir=".", interactive=True),
    Command("RSS", ["/usr/bin/make", "install"],
            workdir="~/trial/rdfconv", interactive=True),
    Command("Y!", ["/usr/bin/make", "install"],
            workdir="~/public_html/geocities", interactive=True),
    DirList,
    ))



