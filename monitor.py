#!/usr/bin/python
# coding=UTF-8

import subprocess

from gi.repository import (GLib, GObject)
from gi.repository import Gdk
from gi.repository import Gtk, Pango
#from gi.repository.GdkPixbuf import Pixbuf

from procbar import ExecViewBase
from misc import GtkUtils, FileUtils


class CommandToolbar(Gtk.Toolbar):
    PRESET_WORDS = (
        'Warning', 'Error',
    )

    TOOLBAR_CSS = Gtk.CssProvider()
    TOOLBAR_CSS.load_from_data("""
        .toolbar {
            /* background: rgb(250.0, 0.0, 0.0); */
            padding: 0;
        }
    """)

    WRAP_MODES = Gtk.ListStore(str, object)
    WRAP_MODES.append(["None", Gtk.WrapMode.NONE])
    WRAP_MODES.append(["Char", Gtk.WrapMode.CHAR])
    WRAP_MODES.append(["Word", Gtk.WrapMode.WORD])
    DEFAULT_WRAP_MODE = 1 # CHAR

    __gsignals__ = {
        'focus-search': (GObject.SIGNAL_ACTION, GObject.TYPE_NONE, ()),
        'search-forward': (GObject.SIGNAL_ACTION, GObject.TYPE_NONE, ()),
        'search-backward': (GObject.SIGNAL_ACTION, GObject.TYPE_NONE, ()),
    }

    def __init__(self):
        super(CommandToolbar, self).__init__()
        self._textbuffer = None
        self._word_list = None
        self._case = None

        ctx = self.get_style_context()
        #ctx.add_class(Gtk.STYLE_CLASS_INLINE_TOOLBAR)
        #print ctx.lookup_color("theme-bg-color")[1].to_string()

        ctx.add_provider(CommandToolbar.TOOLBAR_CSS,
                                Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_SAVE)
        button.set_tooltip_text("Save to file PID.out")
        button.connect("clicked", self._save)
        self.insert(button, -1)

        button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_FILE)
        button.set_tooltip_text("Append to file PID.out and clear output text")
        button.connect("clicked", self._append_and_clear)
        self.insert(button, -1)

        button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_DELETE)
        button.set_tooltip_text("Delete output text")
        button.connect("clicked",
                lambda *args: self._outview.output_buffer().clear_output())
        self.insert(button, -1)

        button = Gtk.ToggleToolButton.new_from_stock(Gtk.STOCK_EDIT)
        button.set_tooltip_text("Toggle auto scrolling and allow edit")
        button.connect("clicked", self._toggle_editable)
        self._editable = button
        self.insert(button, -1)

        self.insert(Gtk.SeparatorToolItem(), -1)

        combo = self.__create_search_entry()
        item = self._shrinked_item(combo)
        self.insert(item, -1)
        self._entry = combo.get_child()

        button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_FIND)
        button.connect("clicked", self._search_text)
        self.insert(button, -1)

        item = Gtk.ToolItem()
        button = Gtk.CheckButton.new_with_mnemonic("_Case")
        button.set_tooltip_text("Match case")
        button.set_active(True)
        self._case = button
        item.add(button)
        self.insert(item, -1)

        button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_CLEAR)
        button.set_tooltip_text("Clear all text highlighting")
        button.connect("clicked", self._clear_hilite)
        self.insert(button, -1)

        button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_GO_DOWN)
        #button.set_action_name("tab.search-forward")
        button.set_tooltip_text("Search forward")
        button.connect("clicked", lambda *args: self._highlight_text(True))
        self.insert(button, -1)

        button = Gtk.ToolButton.new_from_stock(Gtk.STOCK_GO_UP)
        #button.set_action_name("tab.search-backward")
        button.set_tooltip_text("Search backward")
        button.connect("clicked", lambda *args: self._highlight_text(False))
        self.insert(button, -1)

        item = Gtk.ToolItem()
        item.add(self._create_wrap_selector())
        self.insert(item, -1)

        self.connect("focus-search", lambda self: self.focus_search())
        self.connect("search-forward", lambda self: self._highlight_text(True))
        self.connect("search-backward",
                                lambda self: self._highlight_text(False))

    def __create_search_entry(self):
        self._word_list = Gtk.ListStore(str)
        combo = Gtk.ComboBox.new_with_model_and_entry(self._word_list)
        combo.set_name("toolbar-entry")
        combo.set_entry_text_column(0)
        entry = combo.get_child()
        entry.set_icon_from_stock(Gtk.EntryIconPosition.SECONDARY,
                        Gtk.STOCK_CLEAR)
        entry.connect("icon-press", self._clear_text)
        entry.connect("activate", self._search_text)
        for i in CommandToolbar.PRESET_WORDS:
            self._word_list.append([i])
        combo.show()

        return combo

    def _shrinked_item(self, widget):
        item = Gtk.ToolItem()
        f = Gtk.Alignment.new(1.0, 0.5, 1.0, 0.0)
        f.set_property("top-padding", 0)
        f.set_property("bottom-padding", 2)
        f.add(widget)
        item.add(f)
        return item

    def _create_wrap_selector(self):

        combo = Gtk.ComboBox.new_with_model(CommandToolbar.WRAP_MODES)
        renderer = Gtk.CellRendererText()
        combo.pack_start(renderer, True)
        combo.add_attribute(renderer, "text", 0)
        combo.set_active(self.DEFAULT_WRAP_MODE)
        combo.connect("changed", self._set_wrap_mode)
        combo.set_tooltip_text("Wrapping mode")

        f = Gtk.Alignment.new(1.0, 0.5, 1.0, 0.0)
        f.set_property("top-padding", 4)
        f.set_property("bottom-padding", 4)
        f.add(combo)

        return f

    def _save(self, *args):
        self._outview.output_buffer().save_as()

    def _append_and_clear(self, *args):
        end = self._outview.output_buffer().save_as(None, "at")
        if end is not None:
            self._outview.output_buffer().clear_output(end)

    def _set_wrap_mode(self, widget):
        idx = widget.get_active()
        choices = widget.get_model()
        row = choices[idx]
        self._outview.set_wrap_mode(row[1])

    def set_output_view(self, view):
        self._outview = view
        self._textbuffer = view._textview.get_buffer()
        self._editable.set_active(view._textview.get_editable())
        self._outview.set_wrap_mode(self.DEFAULT_WRAP_MODE)

    def _toggle_editable(self, widget):
        self._outview.set_editable(widget.get_active())

    def _clear_text(self, *args):
        self._entry.set_text("")

    def _clear_hilite(self, *args):
        self._outview.output_buffer().clear_hilite()

    def _search_text(self, *args):
        qstr = self._target_text()
        if qstr != '':
            self._outview.output_buffer().set_hilite_string(qstr,
                                                        self._search_flags())

    def _target_text(self):
        qstr = self._entry.get_text().strip()
        if qstr != '':
            self._store_search_text(qstr)
        return qstr

    def _search_flags(self):
        flags = Gtk.TextSearchFlags.VISIBLE_ONLY | Gtk.TextSearchFlags.TEXT_ONLY
        if not self._case.get_active():
            flags |= Gtk.TextSearchFlags.CASE_INSENSITIVE
        return flags

    def _store_search_text(self, text):
        GtkUtils.remove_duplicated_row(self._word_list, 0, text)
        self._word_list.prepend([text])

    def focus_search(self):
        self._entry.grab_focus()

    def _highlight_text(self, forward):
        qstr = self._target_text()
        if qstr:
            self._outview.show_next(qstr, self._search_flags(), forward)


class ProcessOutputBuffer(object):
    """Accumulate output of a process in a Textbuffer."""

    #EOF_IMG = Pixbuf.new_from_file_at_size("logo.png", 16, 16)
    # Use Gtk.Widget.render_icon method to create Pixbuf object.
    # Even a Gtk.Invisible object seems to be OK.
    EOF_IMG = Gtk.Invisible().render_icon(Gtk.STOCK_QUIT, Gtk.IconSize.MENU)

    def __init__(self, textbuf):
        super(ProcessOutputBuffer, self).__init__()
        self._textbuf = textbuf
        self._pipe = None
        self._stdout_id = None
        self._stderr_id = None
        self._std_tag = self._textbuf.create_tag("std",
                                font="Mono 10", foreground="white")
        self._err_tag = self._textbuf.create_tag("err",
                                foreground="orange")
        self._alive = 0x0
        self._tip_mark = textbuf.create_mark("tip",
                                textbuf.get_end_iter(), True)
        # For stderr/stdout
        self._rest = [b'', b'']
        self._pattern = None
        self._marked = False    # Currently marked
        # Iterators can't be used to preserve positions
        # across buffer modifications.
        # Use Gtk.TextMark objects instead.
        self._match_start = textbuf.create_mark("match-start", textbuf.get_start_iter(), True)
        self._match_end = textbuf.create_mark("match-end", textbuf.get_start_iter(), False)

        self._old_hilite_tag = self._textbuf.create_tag("old",
            underline=Pango.Underline.SINGLE, weight=Pango.Weight.BOLD,
            foreground="#27408B", background="#7FFFD4") # Aquamarine

        # Create highlight tag after old one. (for priority)
        self._hilite_tag = self._textbuf.create_tag("match",
            underline=Pango.Underline.SINGLE, weight=Pango.Weight.BOLD,
            foreground="black", background="#FFD700")

        self._spot_tag = self._textbuf.create_tag("found",
            underline=Pango.Underline.SINGLE, weight=Pango.Weight.BOLD,
            foreground="white", background="magenta")

    def set_pipe(self, p):
        self._pipe = p
        self._rest = [b'', b'']
        FileUtils.set_blocking_mode(p.stdout.fileno(), False)
        # Since python GI does not offer io_add_watch_full()
        # we cannot specify destroy callback.
        # It seems that watch event is automatically removed
        # when the channel gets closed.
        self._stdout_id = GLib.io_add_watch(p.stdout, GLib.PRIORITY_DEFAULT,
                                GLib.IOCondition.IN | GLib.IOCondition.HUP,
                                self._append_stdout_data)

        FileUtils.set_blocking_mode(p.stderr.fileno(), False)
        self._stderr_id = GLib.io_add_watch(p.stderr, GLib.PRIORITY_DEFAULT,
                                GLib.IOCondition.IN | GLib.IO_HUP,
                                self._append_stderr_data)
        self._alive = ((1 << p.stdout.fileno()) | (1 << p.stderr.fileno()))

    def set_hilite_string(self, qstr, flags=0):
        if qstr != self._pattern:
            self._pattern = (qstr, flags)
            self.clear_hilite(False)
        self.search_and_mark(qstr, flags)

    def search_and_mark(self, text, flags, start=None):
        if start is None:
            start = self._textbuf.get_start_iter()
        end = self._textbuf.get_end_iter()

        while True:
            match = start.forward_search(text, flags, end)
            if match is None:
                break
            match_start, match_end = match
            # Apply both old and current tags.
            self._textbuf.apply_tag(self._hilite_tag, match_start, match_end)
            self._textbuf.apply_tag(self._old_hilite_tag, match_start, match_end)
            start = match_end

    def next_iter(self, text, flags, forward):
        it = None
        (start, end) = self._search_range(text, flags, forward)

        # Pair of iterators or None
        match = start.forward_search(text, flags, end) if forward \
                    else end.backward_search(text, flags, start)

        if match:
            if self._marked:
                its = self._textbuf.get_iter_at_mark(self._match_start)
                ite = self._textbuf.get_iter_at_mark(self._match_end)
                self._textbuf.remove_tag(self._spot_tag, its, ite)
            self._marked = True

            self._textbuf.apply_tag(self._spot_tag, match[0], match[1])
            self._textbuf.move_mark(self._match_start, match[0])
            self._textbuf.move_mark(self._match_end, match[1])
            it = match[0]

        return it

    def _search_range(self, text, flags, forward):
        start = None
        end = None
        if self._marked:
            if forward:
                start = self._textbuf.get_iter_at_mark(self._match_end)
            else:
                end = self._textbuf.get_iter_at_mark(self._match_start)

        if start is None:
            start = self._textbuf.get_start_iter()

        if end is None:
            end = self._textbuf.get_end_iter()

        return (start, end)

    def clear_hilite(self, both=True):
        start = self._textbuf.get_start_iter()
        end = self._textbuf.get_end_iter()
        self._textbuf.remove_tag(self._spot_tag, start, end)
        self._textbuf.remove_tag(self._hilite_tag, start, end)
        if both:
            # Clear old highlight
            self._textbuf.remove_tag(self._old_hilite_tag, start, end)

    def _append_stdout_data(self, fh, iocond):
        return self._append_data(fh, iocond, True)

    def _append_stderr_data(self, fh, iocond):
        return self._append_data(fh, iocond, False, self._err_tag)

    def _append_data(self, fh, iocond, isstdout, *tags):
        cont = False
        if iocond & GLib.IOCondition.IN:
            text = self._read_text(fh, isstdout)
            if text is not None:
                if text != b'':
                    cont = True
                    self._append_text(text, *tags)
                else:
                    # Probably Unreachable and even if reached
                    print "Got EOF from IO Channel"
                    if (iocond & GLib.IOCondition.HUP) == 0:
                        # This should be unnecessary
                        self._mark_eof(fh.fileno())

        if iocond & GLib.IOCondition.HUP:
            self._mark_eof(fh.fileno())

        if not cont:
            if self._rest[isstdout] != b'':
                print("Remain incomplete bytes:", self._rest[isstdout])
            if isstdout:
                self._stdout_id = None
            else:
                self._stderr_id = None

        return cont

    def _read_text(self, fh, isstdout):
        text = None

        data = fh.read()
        if data is not None:
            if data != b'':
                text = self._adjust_text(data, isstdout)
            else:
                # EOF
                text = b''
        else:
            # None means EAGAIN - try later
            print ">>Output data not ready"

        return text

    def _adjust_text(self, text, isstdout):
        # Remove trailing incomplete character from bytes.

        # Prepend previous partial character.
        text = self._rest[isstdout] + text
        self._rest[isstdout] = b""
        try:
            # To detect incomplete UTF-8 character,
            # try to decode whole the text.
            text.decode("UTF-8")
        except UnicodeError as ex:
            if len(text) == ex.end:
                (text, self._rest[isstdout]) = (text[:ex.start], text[ex.start:])
            else:
                print("**** Unexpected Unicode error", ex)
        return text

    def _append_text(self, text, *tags):
        it = self._textbuf.get_end_iter()
        self._textbuf.insert_with_tags(it, text, self._std_tag, *tags)
        #self.emit("text-appended", self._tip_mark)

        if self._pattern is not None:
            self._hilite_pattern()
        self._textbuf.move_mark(self._tip_mark, it)

    def _hilite_pattern(self):
        start = self._textbuf.get_iter_at_mark(self._tip_mark)
        # Rewind to the start of the line assuming that
        # search pattern does not include newline.
        start.set_line_offset(0)
        self.search_and_mark(self._pattern[0], self._pattern[1], start)

    def tip_mark(self):
        return self._tip_mark

    def _mark_eof(self, fno):
        if self._alive:
            self._alive &= ~(1 << fno)
            if self._alive == 0x0:
                it = self._textbuf.get_end_iter()
                self._textbuf.insert_pixbuf(it, ProcessOutputBuffer.EOF_IMG)
                it = self._textbuf.get_end_iter()
                self._textbuf.insert(it, "\n")

    def stop_watch(self):
        if self._stdout_id is not None:
            GLib.source_remove(self._stdout_id)
            self._stdout_id = None

        if self._stderr_id is not None:
            GLib.source_remove(self._stderr_id)
            self._stderr_id = None

    def clear_output(self, end=None):
        start = self._textbuf.get_start_iter()
        if end is None:
            # delete with EOF marker
            end = self._textbuf.get_end_iter()
        self._textbuf.delete(start, end)
        # What to do with tip marker?
        self._marked = False
        self._pattern = None

    def save_as(self, path=None, mode="wt", start=None):
        end = None
        if self._pipe is not None:
            if start is None:
                start = self._textbuf.get_start_iter()
            # Exclude trailing EOF marker
            end = self._textbuf.get_iter_at_mark(self._tip_mark)
            if path is None:
                path = "{0}.out".format(self._pipe.pid)
            try:
                with open(path, mode) as fh:
                    fh.write(self._textbuf.get_text(start, end, True))
            except IOError as ex:
                print "IO error:", str(ex)
        return end


class ProcessOutputView(Gtk.Box):
    """Show process output with conrolling widgets."""

    def __init__(self):
        super(ProcessOutputView, self).__init__(
                orientation=Gtk.Orientation.VERTICAL)
        self._handler_id = None
        self._toolbar = CommandToolbar()
        self.pack_start(self._toolbar, False, False, 1)

        scrollwin = self._create_textview()
        self.pack_start(scrollwin, True, True, 1)
        self._textview = scrollwin.get_child()
        self._out_buffer = ProcessOutputBuffer(self._textview.get_buffer())

        self.set_editable(False)
        self._toolbar.set_output_view(self)

    def _create_textview(self):
        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_hexpand(True)
        scrolledwindow.set_vexpand(True)

        textview = Gtk.TextView()
        rgba = Gdk.RGBA(0.0, 0.0, 0.6, 1.0)
        textview.override_background_color(Gtk.StateFlags.NORMAL, rgba)
        rgba = Gdk.RGBA(0.0, 0.6, 0.6, 1.0)
        textview.override_background_color(Gtk.StateFlags.SELECTED, rgba)
        textview.set_left_margin(4)
        scrolledwindow.add(textview)

        return scrolledwindow

    def set_editable(self, editable):
        self._textview.set_editable(editable)
        self._textview.set_cursor_visible(editable)
        self.set_auto_scroll(not editable)

    def set_wrap_mode(self, mode):
        self._textview.set_wrap_mode(mode)

    def on_justify_toggled(self, widget, justification):
        self._textview.set_justification(justification)

    def output_buffer(self):
        return self._out_buffer

    def show_next(self, text, flags, forward):
        pos = self._out_buffer.next_iter(text, flags, forward)
        if pos is not None:
            #print "Next:", pos.get_line(), pos.get_line_offset()
            self._textview.scroll_to_iter(pos, 0.0, False, 0, 0)

    def set_auto_scroll(self, auto):
        if auto:
            if self._handler_id is None:
                self._handler_id = self._textview.get_buffer().connect(
                        "insert-text", self._scroll_to)
        elif self._handler_id is not None:
            self._textview.get_buffer().disconnect(self._handler_id)
            self._handler_id = None

    def _scroll_to(self, textbuffer, it, text, length, *args):
        self._textview.scroll_to_mark(self._out_buffer.tip_mark(),
                                        0.0, False, 0.0, 0.0)

    def key_bindings(self, binds):
        accgrp = Gtk.AccelGroup()
        for key, act in binds.iteritems():
            keycode, mod = Gtk.accelerator_parse(key)
            self._toolbar.add_accelerator(act, accgrp,
                    keycode, mod, Gtk.AccelFlags.VISIBLE)
        return accgrp


class CommandExecView(ExecViewBase):
    """Run command in a TextView."""

    KeyMap = {
        "<Control>F": "focus-search",
        "<Control>N": "search-forward",
        "<Control><Shift>N": "search-backward",
    }

    def __init__(self):
        super(CommandExecView, self).__init__()
        view = ProcessOutputView()
        self._buffer = view.output_buffer()

        self._exec_win = view

    def monitor_widget(self):
        return self._exec_win

    def stop_monitoring(self):
        self._buffer.stop_watch()

    def start_command(self, cmd):
        self._cmd = cmd
        stdin = cmd._stdin
        if stdin is None:
            # Prevent from suspending with "tty input".
            stdin = subprocess.PIPE

        p = subprocess.Popen(cmd.adapted_command(), cwd=cmd._workdir,
                stdin=stdin, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if cmd._stdin is None:
            # Close stdin. Avoid the process to hang up
            # by waiting for input from stdin
            p.stdin.close()
        self._buffer.set_pipe(p)
        return p.pid

    def clear_output(self):
        self._buffer.clear_output()

    def key_bindings(self):
        return self._exec_win.key_bindings(CommandExecView.KeyMap)
