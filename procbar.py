#!/usr/bin/python
# coding=UTF-8

import sys
import os
import signal

from gi.repository import GObject
from gi.repository import Gdk
from gi.repository import Gtk

from info import CommandInfoPanel


class ProcessStatusBar(Gtk.Box):

    Signals = dict((i[3:], getattr(signal, i)) for i in dir(signal)
                        if i.startswith("SIG") and i.isalnum())
    # Use str.isalnum() to exclude SIG_IGN and SIG_DFL.
    for sig in ("RTMIN", "RTMAX"):
        Signals.pop(sig)

    MainSignals = ('TERM', 'INT', 'HUP', 'KILL', 'ABRT', 'QUIT',
                        'USR1', 'USR2', 'STOP', 'CONT',)

    __gsignals__ = {
        # (timing, return type, args)
        'child-started': (GObject.SIGNAL_RUN_LAST, None, (int,)),
        'child-exited': (GObject.SIGNAL_RUN_LAST, None, (int, int)),
    }

    InfoPanel = CommandInfoPanel()

    def __init__(self, view):
        super(ProcessStatusBar, self).__init__(orientation=Gtk.Orientation.HORIZONTAL)
        self._view = view
        self._cmd = None
        self._pid = None
        self._result = None
        self._kid_id = None
        self._pid_button = Gtk.Button("pid")
        self._pid_image = Gtk.Image()
        self.pack_start(self._pid_image, False, False, 1)
        self.pack_start(self._pid_button, False, False, 1)

        self.pack_start(self._create_signal_menu(), False, False, 1)

        self._stat_image = Gtk.Image()
        self.pack_start(self._stat_image, False, False, 1)
        self._pid_stat = Gtk.Label("Status")
        self.pack_start(self._pid_stat, False, False, 1)
        #self._pid_stat.connect("clicked", self._toggle_visibility)
        self._pid_button.connect("button-press-event", self._toggle_visibility)
        #self._pid_button.connect("button-release-event", self._hide_command)
        # Want to popdown when other tab is selected.
        self._pid_button.connect("focus-out-event", self._hide_command)
        self._visible = False

        self._button = self._create_restart_button(True)
        self.pack_end(self._button, False, False, 1)

        self._add_button = self._create_restart_button(False)
        self.pack_end(self._add_button, False, False, 1)

        self._terminate = self._create_terminate_button()
        self.pack_end(self._terminate, False, False, 1)

    def _toggle_visibility(self, widget, event, *args):
        if self._visible:
            self._hide_command(widget, event)
        else:
            self._show_command(widget, event)

    def _show_command(self, widget, event, *args):
        self.InfoPanel.set_position(Gtk.WindowPosition.MOUSE)
        #self.InfoPanel.set_transient_for(self.InfoPanel)
        self.InfoPanel.set_type_hint(Gdk.WindowTypeHint.DIALOG)
        self.InfoPanel.popup(event, self._cmd, self._pid, self._result)
        self._visible = True

    def _hide_command(self, *args):
        self.InfoPanel.popdown()
        self._visible = False

    def _create_signal_menu(self):
        bar = Gtk.MenuBar()
        sig_item = Gtk.ImageMenuItem.new_from_stock(Gtk.STOCK_CANCEL, None)
        sig_item.set_label("signal")
        sig_item.set_always_show_image(True)

        sigmenu = Gtk.Menu()

        for name in ProcessStatusBar.MainSignals:
            sigmenu.append(self._signal_menu_item(name))

        # Other signals
        sigsubmenu = Gtk.Menu()
        for (name, sig) in sorted(ProcessStatusBar.Signals.iteritems(),
                        key=lambda x: x[1]):
            if name in ProcessStatusBar.MainSignals:
                continue
            sigsubmenu.append(self._signal_menu_item(name, sig))

        item = Gtk.MenuItem("Other")
        item.set_submenu(sigsubmenu)
        sigmenu.append(item)

        sig_item.set_submenu(sigmenu)
        bar.append(sig_item)
        bar.show_all()
        self._sigmenu = sig_item
        sig_item.set_sensitive(False)

        return bar

    def _signal_menu_item(self, name, val=None):

        if val is None:
            val = ProcessStatusBar.Signals[name]

        item = Gtk.MenuItem("{0} ({1:d})".format(name, val))
        item.connect("activate", self._send_signal, val)
        return item

    def _create_terminate_button(self):
        button = Gtk.Button()
        img = Gtk.Image.new_from_stock(Gtk.STOCK_CANCEL, Gtk.IconSize.MENU)
        button.add(img)

        button.set_sensitive(False)
        button.connect("clicked",
                lambda button: self._send_signal(None, signal.SIGTERM))
        button.set_tooltip_text("Send SIGTERM")
        return button

    def _create_restart_button(self, clear):
        button = Gtk.Button()
        if clear:
            stock = Gtk.STOCK_REDO
            tiptext = "Clear output text and restart process"
        else:
            stock = Gtk.STOCK_ADD
            tiptext = "Preserve output text and restart process"
        img = Gtk.Image.new_from_stock(stock, Gtk.IconSize.MENU)
        button.add(img)

        button.set_sensitive(False)
        button.connect("clicked",
                lambda button, *args: self._view.restart(clear))
        button.set_tooltip_text(tiptext)
        return button

    def _send_signal(self, menu, sig):
        try:
            os.kill(self._pid, sig)
        except IOError as ex:
            print "Kill", self._pid, sig, ":", ex

    def set_pid(self, pid, cmd):
        self._pid = pid
        self._cmd = cmd
        self._pid_image.set_from_stock(Gtk.STOCK_YES, Gtk.IconSize.MENU)
        self._pid_button.set_label("Pid: {0:d}".format(pid))
        self._button.set_sensitive(False)
        self._stat_image.set_from_stock(Gtk.STOCK_DIALOG_INFO, Gtk.IconSize.MENU)
        self._pid_stat.set_text("Running")
        self._kid_id = GObject.child_watch_add(pid,
                lambda pid, stat, *args: self.set_code(stat))
        cmdline = ProcessStatusBar.cmdline(pid)
        if cmdline is None:
            cmdline = ("?",)
        self._pid_button.set_tooltip_text(" ".join(cmdline))
        self._toggle_wigets(True)
        self.emit("child-started", pid)

    def _toggle_wigets(self, alive):
        self._sigmenu.set_sensitive(alive)
        self._terminate.set_sensitive(alive)
        self._button.set_sensitive(not alive)
        self._add_button.set_sensitive(not alive)

    @staticmethod
    def cmdline(pid):
        args = None
        path = "/proc/{0:d}/cmdline".format(pid)
        try:
            with open(path, "rb") as fh:
                cmd = fh.read(-1)
        except IOError as ex:
            sys.stderr.write("Cannot read \"{0}\": {1:s}\n".format(path, ex))
        else:
            args = cmd.split(b"\x00")
            # Discard the last empty string
            args.pop()
        return args

    def set_code(self, stat):
        (rc, sig, core) = ProcessStatusBar._decompose(stat)
        self._result = (rc, sig, core)
        msg = []
        if rc is not None:
            msg.append("Exit status {0}".format(rc))
        if sig is not None:
            msg.append("Terminated by signal {0}".format(sig))
        if core:
            msg.append("Core dumped")
        self._pid_stat.set_text(', '.join(msg))
        # Popdown menu
        self._sigmenu.get_submenu().deactivate()
        self._toggle_wigets(False)
        self._kid_id = None
        self.emit("child-exited", self._pid, stat)
        self._pid_image.set_from_stock(Gtk.STOCK_NO, Gtk.IconSize.MENU)
        if rc != 0 or sig is not None:
            self._stat_image.set_from_stock(Gtk.STOCK_DIALOG_WARNING,
                                                Gtk.IconSize.MENU)

    def stop_watch(self):
        if self._kid_id is not None:
            GObject.source_remove(self._kid_id)
            self._kid_id = None
            # To avoid zombie, simply ignore the result
            GObject.child_watch_add(self._pid, lambda pid, stat, *args: True)

    def is_running(self):
        return self._kid_id is not None

    @staticmethod
    def _decompose(stat):
        rc = None
        sig = None
        core = ((stat & 0x80) != 0)

        low7 = (stat & 0x7f);
        #sigtermed = (low7 != 0 && low7 != 0x7f)
        if low7 == 0:
            rc = (stat >> 8) & 0xff
        elif low7 != 0x7f:
            # Terminated by a signal
            sig = low7

        return (rc, sig, core)


class ExecViewBase(object):
    """Run command and show its output."""
    def monitor_widget(self):
        return None

    def stop_monitoring(self):
        pass

    def start_command(self, cmd):
        return None # PID

    def clear_output(self):
        pass

    def key_bindings(self):
        # Gtk.AccelGroup()
        return None


class CommandView(Gtk.Box):
    """Command output monitoring window with information bar."""

    def __init__(self, name, monitor):
        super(CommandView, self).__init__(orientation=Gtk.Orientation.VERTICAL)
        self._statbar = ProcessStatusBar(self)
        self.pack_start(self._statbar, False, False, 1)
        self._name = name
        self._exec_win = monitor
        self.pack_start(self._exec_win.monitor_widget(), True, True, 1)
        self._cmd = None

    def name(self):
        return self._name

    def set_status_change_callbacks(self, startedcb, exitedcb):
        self._statbar.connect("child-started", startedcb)
        self._statbar.connect("child-exited", exitedcb)

    def stop_monitoring(self):
        self._statbar.stop_watch()
        self._exec_win.stop_monitoring()

    def is_running(self):
        return self._statbar.is_running()

    def start_command(self, cmd):
        self._cmd = cmd
        pid = self._exec_win.start_command(cmd)
        self._statbar.set_pid(pid, cmd)

    def restart(self, clear):
        if clear:
            self._exec_win.clear_output()
        self.start_command(self._cmd)
