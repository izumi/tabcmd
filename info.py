#!/usr/bin/python
# coding=UTF-8

from gi.repository import Gtk

from command import Command


class CommandInfo(Gtk.Grid):
    """docstring for CommandInfo"""
    def __init__(self):
        super(CommandInfo, self).__init__()
        self._create()

    def _create(self):
        for i, n in enumerate(("Name", "Args", "Work dir", "Environ",
                "Interactive", "Buffering", "-",
                "Pid", "Exit status", "Signal", "Core")):
            if n == "-":
                self.attach(Gtk.Separator(), 0, i, 2, 1)
            else:
                label = Gtk.Label(n)
                self.attach(label, 0, i, 1, 1)
                label.set_alignment(0.5, 0.04)

        row = 0
        self._name = Gtk.Label("a name")
        self.attach(self._name, 1, row, 1, 1)
        row += 1

        self._args = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.attach(self._args, 1, row, 1, 1)
        row += 1

        self._workdir = Gtk.Label("a work dir")
        self._workdir.set_property("selectable", True)
        self._workdir.set_property("can-focus", False)
        self.attach(self._workdir, 1, row, 1, 1)
        row += 1

        # Environ
        row += 1

        self._interacitive = Gtk.Label("t/f")
        self.attach(self._interacitive, 1, row, 1, 1)
        row += 1

        buffering = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self._stdout = Gtk.Label("stdout")
        buffering.pack_start(self._stdout, False, True, 1)
        self._stderr = Gtk.Label("stderr")
        buffering.pack_start(self._stderr, False, True, 1)
        self.attach(buffering, 1, row, 1, 1)
        row += 1

        # Separator
        row += 1

        self._pid = Gtk.Label("0")
        self.attach(self._pid, 1, row, 1, 1)
        self._pid.set_property("selectable", True)
        self._pid.set_property("can-focus", False)
        row += 1

        self._exit = Gtk.Label("0")
        self.attach(self._exit, 1, row, 1, 1)
        row += 1

        self._signal = Gtk.Label("0")
        self.attach(self._signal, 1, row, 1, 1)
        row += 1

        self._core = Gtk.Label("t/f")
        self.attach(self._core, 1, row, 1, 1)
        row += 1

        self.set_property("column-spacing", 4)

    def show_command(self, cmd, pid, result):
        self._name.set_text(cmd._name)
        labels = self._args.get_children()
        for label in labels:
            self._args.remove(label)
        for arg in cmd._cmdline:
            label = Gtk.Label(arg)
            self._args.pack_start(label, False, True, 1)
            label.set_alignment(0.04, 0.5)
        self._workdir.set_text(cmd._workdir)
        self._interacitive.set_text(str(cmd._interactive))

        bufsiz = cmd._outbuf
        if bufsiz == Command.BUF_LINE:
            bufsiz = "Line"
        elif cmd._outbuf == Command.BUF_INTACT:
            bufsiz = "None"
        self._stdout.set_text("Stdout: {0}".format(bufsiz))

        bufsiz = cmd._errbuf
        if bufsiz == Command.BUF_LINE:
            bufsiz = "Line"
        elif cmd._outbuf == Command.BUF_INTACT:
            bufsiz = "None"
        self._stderr.set_text("Stderr: {0}".format(bufsiz))

        self._pid.set_text(str(pid))
        (rc, sig, core) = result if result is not None else (None, None, None)
        if rc is None:
            rc = "-"
        if sig is None:
            sig = ""
        self._exit.set_text(str(rc))
        self._signal.set_text(str(sig))
        self._core.set_text("dumped" if core else "")

        self.show_all()


class XCommandInfoPanel(Gtk.Dialog):
    def __init__(self, parent=None):
        super(CommandInfoPanel, self).__init__("Command Info", parent,
                        Gtk.DialogFlags.DESTROY_WITH_PARENT,
                        (Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE))

        container = self.get_content_area()
        container.add(Gtk.Label("Hello"))
        container.show_all()
        self.connect("close", self.popdown)
        self.connect("delete-event", self.popdown)

    def popup(self, cmd):
        self.set_decorated(False)
        self.show()

    def popdown(self, *args):
        self.hide()
        return True # Preserve (do not destroy) this window


class CommandInfoPanel(Gtk.Window):
    def __init__(self, parent=None):
        super(CommandInfoPanel, self).__init__(Gtk.WindowType.POPUP)

        self.set_default_size(100, 200)
        self._info = CommandInfo()
        self.add(self._info)
        #self.connect("close", self.popdown)
        #self.connect("delete-event", self.popdown)

    def popup(self, event, cmd, pid, result):
        #self.set_decorated(False)
        self.move(event.x_root + 10, event.y_root + 10)
        self._info.check_resize()
        self.check_resize()
        self._info.show_command(cmd, pid, result)
        self.show_all()

    def popdown(self, *args):
        self.hide()
        return True # Preserve (do not destroy) this window
